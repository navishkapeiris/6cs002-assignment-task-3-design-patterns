package view;
import model.AbominodoModel;
import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.util.Observable;
import java.util.Observer;
import java.util.List;

@SuppressWarnings("deprecation")
public class AbominodoView implements Observer {
    private JFrame frame;
    private JButton[][] buttons;

    public AbominodoView() {
        frame = new JFrame("Abominodo");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setLayout(new GridLayout(4, 4));

        initializeButtons();
        addButtonsToFrame();

        frame.pack();
        frame.setLocationRelativeTo(null);
        frame.setVisible(true);
    }

    private void initializeButtons() {
        buttons = new JButton[4][4];
        for (int i = 0; i < 4; i++) {
            for (int j = 0; j < 4; j++) {
                buttons[i][j] = new JButton();
                buttons[i][j].addActionListener(e -> handleButtonClick(e));
                frame.add(buttons[i][j]);
            }
        }
    }

    private void addButtonsToFrame() {
        
    }

    private void handleButtonClick(ActionEvent event) {
        JButton clickedButton = (JButton) event.getSource();
        int row = -1, col = -1;
        for (int i = 0; i < 4; i++) {
            for (int j = 0; j < 4; j++) {
                if (buttons[i][j] == clickedButton) {
                    row = i;
                    col = j;
                    break;
                }
            }
        }
        
    }

	@Override
    public void update(Observable o, Object arg) {
        if (o instanceof AbominodoModel) {
            AbominodoModel model = (AbominodoModel) o;
            updateView(model);
        }
    }

    private void updateView(AbominodoModel model) {
        List<List<Integer>> gameBoard = model.getGameBoard();
        for (int i = 0; i < 4; i++) {
            for (int j = 0; j < 4; j++) {
      
            }
        }
    }
}
