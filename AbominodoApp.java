package main;
import controller.AbominodoController;
import model.AbominodoModel;
import view.AbominodoView;

public class AbominodoApp {
    @SuppressWarnings("deprecation")
	public static void main(String[] args) {
        AbominodoModel model = new AbominodoModel();
        AbominodoView view = new AbominodoView();
        AbominodoController controller = new AbominodoController(model);

        model.addObserver(view);
        model.addObserver(controller);
    }
}

