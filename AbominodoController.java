package controller;
import model.AbominodoModel;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Observable;
import java.util.Observer;

import javax.swing.JButton;

public class AbominodoController implements ActionListener, Observer {
    private AbominodoModel model;

    public AbominodoController(AbominodoModel model) {
        this.model = model;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getSource() instanceof JButton) {
            JButton clickedButton = (JButton) e.getSource();
            int row = 4/* calculate row based on clickedButton */;
            int col = 4/* calculate col based on clickedButton */;
            model.makeMove(row, col);
        }
    }

    @Override
    public void update(Observable o, Object arg) {
    }
}
