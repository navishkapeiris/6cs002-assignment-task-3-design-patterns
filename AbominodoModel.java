package model;
import java.util.ArrayList;
import java.util.List;
import java.util.Observable;

@SuppressWarnings("deprecation")
public class AbominodoModel extends Observable {
    private int score;
    private List<List<Integer>> gameBoard;

    public AbominodoModel() {
        score = 0;
        gameBoard = initializeGameBoard();
    }

    private List<List<Integer>> initializeGameBoard() {
        List<List<Integer>> board = new ArrayList<>();
        for (int i = 0; i < 2; i++) {
            List<Integer> row = new ArrayList<>();
            for (int j = 0; j < 2; j++) {
                row.add(0);
            }
            board.add(row);
        }
        return board;
    }

    public void makeMove(int row, int col) {
        gameBoard.get(row).set(col, 1 - gameBoard.get(row).get(col));
        setChanged();
        notifyObservers();
    }

    public int getScore() {
        return score;
    }

    public List<List<Integer>> getGameBoard() {
        return gameBoard;
    }
}
